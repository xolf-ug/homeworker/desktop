# Homeworker Desktop
A desktop app for [homeworker.li](https://homeworker.li)

### Start
To start the app (development) run `npm run start`

### Package
To package the app (produktion) run the following commands:
- MacOS: `npm run package-mac`
- Windows: `npm run package-windows`